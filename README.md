# go-fixedmap

This package provides a simple fixed-size hash map in Go. It simply wraps a Go map in a struct that also
tracks elements adding by use of a linked list. THIS IS NOT THREADSAFE, wrap in a mutex if you need to,
and note that all Get/Put/Remove operations can involve writes.