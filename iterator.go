package fixedmap

import "container/list"

// Iterator provides an ordered iterator over the Elements in a FixedMap
type Iterator struct {
	next *list.Element
	elem *Element
}

// Next attempts to prep the next value in the FixedMap for access
// returning success as to whether there is another value
func (i *Iterator) Next() bool {
	if i.next == nil {
		i.elem = nil
		return false
	}
	i.elem = i.next.Value.(*Element)
	i.next = i.next.Next()
	return true
}

// Element returns the stored next Element from the FixedMap
func (i *Iterator) Element() *Element {
	return i.elem
}
