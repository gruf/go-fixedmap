package fixedmap

import (
	"container/list"
)

// Element defines a key-value pair in the FixedMap
type Element struct {
	Key   interface{}
	Value interface{}
}

// FixedMap is map of fixed size, useful as an LRU cache
type FixedMap struct {
	hash map[interface{}]*list.Element
	list *list.List
	size int
}

// New returns a newly initialized FixedMap instance
func New(size int) FixedMap {
	return FixedMap{
		// size is 1 greater, for moment when new added but old not purged
		hash: make(map[interface{}]*list.Element, size+1),
		list: list.New(),
		size: size,
	}
}

// Len returns the length of the FixedMap
func (fm *FixedMap) Len() int {
	return fm.list.Len()
}

// Cap returns the capacity of the FixedMap
func (fm *FixedMap) Cap() int {
	return fm.size
}

// Get attempts to fetch a value for given key from the map
func (fm *FixedMap) Get(key interface{}) (interface{}, bool) {
	// Fetch from map
	elem, ok := fm.hash[key]
	if !ok {
		return nil, false
	}

	// Move to front of list
	fm.list.MoveToFront(elem)

	// Return value
	return elem.Value.(*Element).Value, true
}

// Put places a key-value pair in the map, returning an Element if
// the map size was reached and the LRU pair had to be evicted
func (fm *FixedMap) Put(key, value interface{}) *Element {
	// Check if we already have key
	elem, ok := fm.hash[key]
	if ok {
		// We need to update value of element
		elem.Value.(*Element).Value = value

		// Move to front
		fm.list.MoveToFront(elem)

		// Return nothing
		return nil
	} else {
		// Add new to list (wrapped as mapElement)
		elem = fm.list.PushFront(&Element{
			Key:   key,
			Value: value,
		})

		// Add to map
		fm.hash[key] = elem

		// If we have exceeded size...
		if fm.list.Len() > fm.size {
			// Get last elem
			elem = fm.list.Back()

			// Get our wrapped type
			evicted := elem.Value.(*Element)

			// Delete from map and list
			delete(fm.hash, evicted.Key)
			fm.list.Remove(elem)

			// Return this evicted element
			return evicted
		}

		// Return nothing
		return nil
	}
}

// Has checks if the FixedMap contains supplied key
func (fm *FixedMap) Has(key interface{}) bool {
	// Simply check map
	_, ok := fm.hash[key]
	return ok
}

// Remove will remove an element from the map with given key
func (fm *FixedMap) Remove(key interface{}) {
	// If we can't find key, return
	elem, ok := fm.hash[key]
	if !ok {
		return
	}

	// Get our wrapped type
	removed := elem.Value.(*Element)

	// Delete entry from map and list
	delete(fm.hash, removed.Key)
	fm.list.Remove(elem)
}

// PopFront pops the elment at the front of the map
func (fm *FixedMap) PopFront() *Element {
	// If len < 1, nothing to do
	if fm.Len() < 1 {
		return nil
	}

	// Get first element from list
	first := fm.list.Front()

	// Get wrapped element
	elem := first.Value.(*Element)

	// Delete entry from map and list
	delete(fm.hash, elem.Key)
	fm.list.Remove(first)

	// Return the element
	return elem
}

// PopBack pops the last element from the map
func (fm *FixedMap) PopBack() *Element {
	// If len < 1, nothing to do
	if fm.Len() < 1 {
		return nil
	}

	// Get last element from list
	last := fm.list.Back()

	// Get wrapped element
	elem := last.Value.(*Element)

	// Delete entry from map and list
	delete(fm.hash, elem.Key)
	fm.list.Remove(last)

	// Return the element
	return elem
}

// Iterator returns a new prepared Iterator for this map
func (fm *FixedMap) Iterator() *Iterator {
	return &Iterator{
		next: fm.list.Front(),
		elem: nil,
	}
}

// cat girls are stinky
//
// kobolds best
//
// :3c
// ~wife
