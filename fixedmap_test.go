package fixedmap_test

import (
	"testing"

	"codeberg.org/gruf/go-fixedmap"
)

func TestFixedMap(t *testing.T) {
	fm := fixedmap.New(1)
	if v := fm.Put("hello", "world"); v != nil {
		t.Fatalf("unexpectedly evicted element: %v", v)
	}
	v, ok := fm.Get("hello")
	if !ok || v != "world" {
		t.Fatalf("retrieved element was not as expected: %v", v)
	}
	if v := fm.Put("hello2", "world2"); v == nil || v.Key != "hello" || v.Value != "world" {
		t.Fatalf("evicted element was not as expected: %v", v)
	}
	if !fm.Has("hello2") {
		t.Fatalf("could not find expected element in FixedMap")
	}

	// TODO: further testing
}
